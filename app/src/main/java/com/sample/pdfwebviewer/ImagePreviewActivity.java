package com.sample.pdfwebviewer;

import android.app.Activity;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

public class ImagePreviewActivity extends Activity {

    private String imageFilePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_preview);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.containsKey("ImageFilePath")) {
            imageFilePath = bundle.getString("ImageFilePath");

        }
        ImageView imageView = findViewById(R.id.iv_image_preview);
        if (imageFilePath != null) {
            imageView.setImageURI(Uri.parse(imageFilePath));
        }
    }
}
